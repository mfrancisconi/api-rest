<?php if (!defined('BASEPATH')) exit('No direct script allowed');

class UsuariosApp_model extends CI_Model
{
	function insert($q)
	{
		return $this->db->insert('usuarios_app', $q);
	}

	function get($q)
	{
		return $this->db->get_where('usuarios_app', $q);
	}
}
